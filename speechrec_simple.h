#ifndef SPEECHREC_H
#define SPEECHREC_H

#include <QMediaRecorder>
#include <QUrl>
#include <QAudioRecorder>
#include <QObject>

class speechrec : public QObject
{
    Q_OBJECT
public:
    explicit speechrec(QObject *parent = nullptr);
    QUrl file_location = QUrl::fromLocalFile("buchapp_test.raw");

signals:
    void speechSignal(const QString &msg);
public slots:
    void speechSlot(const bool &msg);
private:
    QAudioRecorder *m_audioRecorder = nullptr;
};

#endif // SPEECHREC_H
