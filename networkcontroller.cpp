#include "networkcontroller.h"
#include <QEventLoop>
#include <QNetworkReply>
#include <QNetworkRequest>

NetworkController::NetworkController(QObject *parent) : QObject(parent)
{
    na_manager = new QNetworkAccessManager();
}

void NetworkController::sendToServ(QByteArray data)
{
    QEventLoop loop;
    QString clientId = "6455770";
    na_manager = new QNetworkAccessManager();
    connect(na_manager, SIGNAL(finished(QNetworkReply*)),
            &loop, SLOT(quit()));
    QNetworkReply * reply = na_manager->get(QNetworkRequest(QUrl("https://localhost:13245/api/v0.1"
                                                                 "?data=" + data)));            // Произвольная строка,
    qDebug() << "*** GET before loop.exec()";
    loop.exec();
    qDebug() << "*** GET after loop.exec()";
}
