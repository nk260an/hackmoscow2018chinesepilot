#include "speechrec.h"
#include "QDebug"

speechrec::speechrec(QObject *parent) : QObject(parent)
{


    m_audioRecorder = new QAudioRecorder();

       m_audioRecorder->setOutputLocation(c);
       m_audioRecorder->setAudioInput("Default");

       QAudioEncoderSettings settings;
       settings.setCodec("audio/pcm");
       settings.setSampleRate(16000);
       settings.setBitRate(128000);
       settings.setChannelCount(1);
     //  settings.setQuality(QMultimedia::EncodingQuality(ui->qualitySlider->value()));
       settings.setEncodingMode(QMultimedia::ConstantBitRateEncoding);

       QString container = "audio/x-raw";

       m_audioRecorder->setEncodingSettings(settings, QVideoEncoderSettings(), container);



       config = cmd_ln_init(NULL, ps_args(), TRUE,
                    "-hmm", PRO_FILE_PWD "/lib/cmusphinx-ru-5.2",
                    "-lm", PRO_FILE_PWD "/lib/cmusphinx-ru-5.2/ru.lm",
                    "-dict", PRO_FILE_PWD "/lib/cmusphinx-ru-5.2/ru.dic",
                    NULL);
       if (config == NULL) {
       qDebug() <<"Failed to create config object, see log for details\n";
       }

       ps = ps_init(config);
       if (ps == NULL) {
      qDebug() <<"Failed to create recognizer, see log for details\n";
       }

}

void speechrec::speechSlot(const bool &msg)
{
if(msg){
   m_audioRecorder->record();
}else{
   m_audioRecorder->stop();

   QString str = "test.raw";

       fh = fopen(str.toLocal8Bit().constData(), "rb");
       if (fh == NULL) {
            qDebug() << "not";
       }

       rv = ps_start_utt(ps);

       while (!feof(fh)) {
            size_t nsamp;
            nsamp = fread(buf, 2, 512, fh);
            rv = ps_process_raw(ps, buf, nsamp, FALSE, FALSE);
       }

       rv = ps_end_utt(ps);
       hyp = ps_get_hyp(ps, &score);



 //  qDebug() << QString(hyp);

   emit speechSignal(QString(hyp));

   fclose(fh);
}
}
