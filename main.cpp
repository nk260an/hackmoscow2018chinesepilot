#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#ifdef Q_OS_ANDROID
    #include <QtAndroid>
    #include "andwrapper.h"
#endif
#include "speechrec_simple.h"
#include "NetworkController.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    //QGuiApplication app(argc, argv);
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;

    speechrec sRec;
    NetworkController nController;

    #ifdef Q_OS_ANDROID
        AndWrapper wrp;
    #endif

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;
    #ifdef Q_OS_ANDROID
        QtAndroid::hideSplashScreen(500);
    #endif

    QObject * appWindow = engine.rootObjects().first();

    #ifdef Q_OS_ANDROID
        QObject::connect(appWindow, SIGNAL(makeCall(QString)),
                         &wrp, SLOT(directCall(QString)) );
        QObject::connect(appWindow, SIGNAL(sendSMS(QString, QString)),
                         &wrp, SLOT(sendSMS(QString, QString)) );

        /*QObject::connect(appWindow, SIGNAL(startRecord(bool)),
                         &sRec, SLOT(speechSlot(bool)) );*/
    #endif
        QObject::connect(&sRec, SIGNAL(speechSignal(QByteArray)),
                         &nController, SLOT(sendToServ(QByteArray)) );

    return app.exec();
}
