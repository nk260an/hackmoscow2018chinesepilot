#include "speechrec_simple.h"
#include "QDebug"

speechrec::speechrec(QObject *parent) : QObject(parent)
{


    m_audioRecorder = new QAudioRecorder();

    m_audioRecorder->setOutputLocation(file_location);
    m_audioRecorder->setAudioInput("Default");

    QAudioEncoderSettings settings;
    settings.setCodec("audio/pcm");
    settings.setSampleRate(16000);
    settings.setBitRate(128000);
    settings.setChannelCount(1);
    //  settings.setQuality(QMultimedia::EncodingQuality(ui->qualitySlider->value()));
    settings.setEncodingMode(QMultimedia::ConstantBitRateEncoding);

    QString container = "audio/x-raw";

    m_audioRecorder->setEncodingSettings(settings, QVideoEncoderSettings(), container);

}

void speechrec::speechSlot(const bool &msg)
{
    if(msg){
        m_audioRecorder->record();
    }else{
        m_audioRecorder->stop();
        //emit speechSignal(QString(hyp));
        //fclose(fh);
    }
}
