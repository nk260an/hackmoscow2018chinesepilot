#ifndef SPEECHREC_H
#define SPEECHREC_H

#include <QMediaRecorder>
#include <QUrl>
#include <QAudioRecorder>
#include <pocketsphinx.h>
#include <QObject>

class speechrec : public QObject
{
    Q_OBJECT
public:
    explicit speechrec(QObject *parent = nullptr);

    ps_decoder_t *ps;

    QUrl c = QUrl::fromLocalFile(QString(PRO_FILE_PWD) + "/lib/test.raw");

    cmd_ln_t *config;
    FILE *fh;
    char const *hyp, *uttid;
    int16 buf[512];
    int rv;
    int32 score;

signals:
    void speechSignal(const QString &msg);
public slots:
    void speechSlot(const bool &msg);
private:
    QAudioRecorder *m_audioRecorder = nullptr;
};

#endif // SPEECHREC_H
