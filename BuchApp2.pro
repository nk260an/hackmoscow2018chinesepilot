QT += core gui qml quick network multimedia #webview # charts widgets  webkit webengine
CONFIG += c++11
DEFINES += PRO_FILE_PWD=$$sprintf("\"\\\"%1\\\"\"", $$_PRO_FILE_PWD_)
android: QT += androidextras
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
	main.cpp \
	andwrapper.cpp \
    speechrec_simple.cpp \
    networkcontroller.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    andwrapper.h \
    speechrec_simple.h \
    networkcontroller.h

DISTFILES += \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    android/res/drawable/splash.xml \
    android/res/values/apptheme.xml \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/res/drawable/splashscreen.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

#message($$PWD)

DEFINES += PRO_FILE_PWD=$$sprintf("\"\\\"%1\\\"\"", $$_PRO_FILE_PWD_)


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BoohAppV1_1

#INCLUDEPATH += $$PWD/lib/pocketsphinx/include \
#		$$PWD/lib/sphinxbase/include \
#		$$PWD/lib/sphinxbase/include/win32



#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/pocketsphinx/bin/Release/Win32/ -lsphinxbase
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/pocketsphinx/bin/Release/Win32/ -lsphinxbase
#else:unix:!android: LIBS += -L$$PWD/lib/pocketsphinx/bin/Release/Win32/ -lsphinxbase

#INCLUDEPATH += $$PWD/lib/pocketsphinx/bin/Release/Win32
#DEPENDPATH += $$PWD/lib/pocketsphinx/bin/Release/Win32

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/pocketsphinx/bin/Release/Win32/sphinxbase.lib
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/pocketsphinx/bin/Release/Win32/sphinxbase.lib
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/pocketsphinx/bin/Release/Win32/sphinxbase.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/pocketsphinx/bin/Release/Win32/sphinxbase.lib
#else:unix:!android: PRE_TARGETDEPS += $$PWD/lib/pocketsphinx/bin/Release/Win32/sphinxbase.lib




#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/pocketsphinx/bin/Release/Win32/ -lpocketsphinx
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/pocketsphinx/bin/Release/Win32/ -lpocketsphinx
#else:unix:!android: LIBS += -L$$PWD/lib/pocketsphinx/bin/Release/Win32/ -lpocketsphinx

#INCLUDEPATH += $$PWD/lib/pocketsphinx/bin/Release/Win32
#DEPENDPATH += $$PWD/lib/pocketsphinx/bin/Release/Win32

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/pocketsphinx/bin/Release/Win32/pocketsphinx.lib
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/pocketsphinx/bin/Release/Win32/pocketsphinx.lib
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/pocketsphinx/bin/Release/Win32/pocketsphinx.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/pocketsphinx/bin/Release/Win32/pocketsphinx.lib
#else:unix:!android: PRE_TARGETDEPS += $$PWD/lib/pocketsphinx/bin/Release/Win32/pocketsphinx.lib
