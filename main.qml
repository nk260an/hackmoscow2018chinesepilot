import QtQuick 2.11
import QtQuick.Controls 1.1
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11
import QtQuick.Window 2.11
import QtSensors 5.11
//import QtCharts 2.0
//import QtWebKit 3.0
//import QtWebEngine 1.7

//import QtWebView 1.1

ApplicationWindow {
    id : mainWindow
    visible: true
    width: 480
    height: 640
    title: qsTr("Tabs")

    signal startRecord(bool state);
    //signal stopRecord();
    signal makeCall(string phonenumber);
    signal sendSMS(string phonenumber, string message);

    header: Rectangle{ // заголовок окна
        color: "blue"
        width: parent.width
        height: 50

        Image{ // инонка кружки
            id : imgHeader
            height: 40
            width: 40
            source: "qrc:/img/emo.png"
            anchors.left: parent.left
            anchors.right: lblHeader.left
            anchors.verticalCenter: parent.verticalCenter
        }

        Label{ // "BOOHSCREEN"
            id : lblHeader
            text : "BOOHOSCOPE"
            font.bold: true
            font.pixelSize: 40
            color: "white"
            anchors.left: imgHeader.right
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: 0
        //currentIndex: tabBar.currentIndex
        interactive: false

        Page{// 1. настройки, соглашение
            ColumnLayout{
                anchors.fill: parent
                CheckBox{
                    text: "I accept terms"
                    font.pixelSize: 20
                    Layout.fillWidth: true
                    Layout.preferredHeight: 40
                }
                CheckBox{
                    text : "I agreed to improve"
                    font.pixelSize: 20
                    checked: true
                    Layout.fillWidth: true
                    Layout.preferredHeight: 40
                }
                Slider{ //слайдер степеней опъянения
                    snapMode : Slider.SnapAlways
                    from : 1
                    to : 3
                    stepSize: 1
                    Layout.fillWidth: true
                    Layout.preferredHeight: 40
                }
                RowLayout{ // подписи слайдера
                    Label{
                        text:"None"
                        font.pixelSize: 20
                        Layout.fillWidth: true
                        Layout.preferredHeight: 40
                        Layout.alignment: Qt.AlignLeft
                        horizontalAlignment: Text.AlignLeft
                    }
                    Label{
                        text:"Moderate"
                        font.pixelSize: 20
                        Layout.fillWidth: true
                        Layout.preferredHeight: 40
                        Layout.alignment: Qt.AlignHCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                    Label{
                        text:"Heavy"
                        font.pixelSize: 20
                        Layout.preferredHeight: 40
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignRight
                        horizontalAlignment: Text.AlignRight
                    }
                }
                Label{ //"State at least 2 sponsors"
                    text: "State at least 2 sponsors"
                    font.pixelSize: 20
                    Layout.fillWidth: true
                    font.bold: true
                    color: "green"
                }
                Item{ // список кому звонить
                    Layout.preferredHeight: 150
                    Layout.fillWidth: true
                    ListView{
                        anchors.fill: parent
                        model: mdl
                        clip: true

                        delegate: Item{
                            id : itm_delegate
                            width: 350
                            height: 49

                            Rectangle{
                                id : rct_delegate
                                anchors.fill: itm_delegate
                                anchors.margins: 2
                                color: "green"
                                radius: 3

                                GridLayout{
                                anchors.fill: rct_delegate
                                Image{
                                    Layout.preferredHeight: 45
                                    Layout.preferredWidth: 45
                                    Layout.alignment: Qt.AlignVCenter
                                    Layout.column: 0
                                    Layout.row: 0
                                    Layout.rowSpan: 2
                                    source: im_srs
                                    verticalAlignment : Image.AlignVCenter
                                    fillMode : Image.PreserveAspectFit
                                    clip: true
                                }
                                Label{
                                    Layout.preferredHeight: 20
                                    Layout.fillWidth: true
                                    Layout.column: 1
                                    Layout.row: 0
                                    text: sname
                                    font.pixelSize: 20
                                    font.bold: true
                                    verticalAlignment: Qt.AlignVCenter
                                }
                                Label{
                                    Layout.preferredHeight: 20
                                    Layout.fillWidth: true
                                    Layout.column: 1
                                    Layout.row: 1
                                    text: fname
                                    font.pixelSize: 15
                                    verticalAlignment: Qt.AlignVCenter
                                }
                            }
                            }
                        }

                        ListModel{
                            id : mdl
                            ListElement{
                                sname : "Кременецкий"
                                fname : "Никита"
                                im_srs:"qrc:/img/nikita.jpg"
                            }
                            ListElement{
                                sname : "Маматенко"
                                fname : "Юрий"
                                im_srs:"qrc:/img/iyrii.jpg"
                            }
                            ListElement{
                                sname : "Чекулаева"
                                fname : "Серафима"
                                im_srs:"qrc:/img/sima.jpg"
                            }
                        }
                    }
                }
                Label{ // "Never let call drunk"
                    text: "Never let call drunk"
                    font.pixelSize: 20
                    Layout.fillWidth: true
                    font.bold: true
                    color: "red"
                }
                Item{ // список, кому звонить нельзя
                    Layout.fillWidth: true
                    Layout.preferredHeight: 100
                    ListView{
                        anchors.fill: parent
                        model: mdl2
                        clip: true

                        delegate: Item{
                            id : itm_delegate2
                            width: 350
                            height: 49

                            Rectangle{
                                id : rct_delegate2
                                anchors.fill: itm_delegate2
                                anchors.margins: 2
                                color: "red"
                                radius: 3

                                GridLayout{
                                anchors.fill: rct_delegate2
                                Image{
                                    Layout.preferredHeight: 45
                                    Layout.preferredWidth: 45
                                    Layout.alignment: Qt.AlignVCenter
                                    Layout.column: 0
                                    Layout.row: 0
                                    Layout.rowSpan: 2
                                    source: im_srs
                                    verticalAlignment : Image.AlignVCenter
                                    fillMode : Image.PreserveAspectFit
                                    clip: true
                                }
                                Label{
                                    Layout.preferredHeight: 20
                                    Layout.fillWidth: true
                                    Layout.column: 1
                                    Layout.row: 0
                                    text: sname
                                    font.pixelSize: 20
                                    font.bold: true
                                    verticalAlignment: Qt.AlignVCenter
                                }
                                Label{
                                    Layout.preferredHeight: 20
                                    Layout.fillWidth: true
                                    Layout.column: 1
                                    Layout.row: 1
                                    text: fname
                                    font.pixelSize: 15
                                    verticalAlignment: Qt.AlignVCenter
                                }
                            }
                            }
                        }

                        ListModel{
                            id : mdl2
                            ListElement{
                                sname : "Boss"
                                fname : ""
                                im_srs:"qrc:/img/boss.png"
                            }
                            ListElement{
                                sname : "BIG"
                                fname : "Boss"
                                im_srs:"qrc:/img/boss2.png"
                            }
                        }
                    }
                }

            }
            footer: Button{
                text : "Next >"
                anchors.right: parent.right
                width: 200
                height: 100
                onClicked: {
                    swipeView.currentIndex += 1;
                }
            }

        }

        Page{// 2. калибровка по точности жестов
            ColumnLayout{
                anchors.fill: parent
                Label{
                    text: "Calibrate your reaction time by touching points as fast as you can"
                    Layout.fillWidth: true
                    Layout.preferredHeight: 100
                    wrapMode: Text.Wrap
                    font.pixelSize: 20
                }
                Item{
                    id : itmButtons
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    property int buttons_left: 4
                    Button{
                        id : btn1
                        height: 100
                        width: 100
                        visible: true
                        x:Math.random()*400
                        y:Math.random()*400

                        onClicked: {
                            if(itmButtons.buttons_left > 1){
                                if(itmButtons.buttons_left == 4){
                                    lblTimeResult.text = "0.000s"
                                    tmr.start();
                                }
                                visible = false
                                btn2.visible = true
                                --itmButtons.buttons_left;
                            }
                            else{
                                btn1.visible = true;
                                itmButtons.buttons_left = 4;
                                tmr.stop();
                                tmr.time_elapsed = 0;
                            }
                        }
                    }
                    Button{
                        id : btn2
                        height: 100
                        width: 100
                        visible: false
                        x:Math.random()*400
                        y:Math.random()*400
                        onClicked: {
                            if(itmButtons.buttons_left > 1){
                                if(itmButtons.buttons_left == 4){
                                    lblTimeResult.text = "0.000s"
                                    tmr.start();
                                }
                                visible = false
                                btn3.visible = true
                                --itmButtons.buttons_left;
                            }
                            else{
                                btn1.visible = true;
                                itmButtons.buttons_left = 4;
                                tmr.stop();
                                tmr.time_elapsed = 0;
                            }
                        }
                    }
                    Button{
                        id : btn3
                        height: 100
                        width: 100
                        x:Math.random()*400
                        y:Math.random()*400
                        visible: false
                        onClicked: {
                            if(itmButtons.buttons_left > 1){
                                if(itmButtons.buttons_left == 4){
                                    lblTimeResult.text = "0.000s"
                                    tmr.start();
                                }
                                visible = false
                                btn4.visible = true
                                itmButtons.buttons_left--;
                            }
                            else{
                                btn1.visible = true;
                                itmButtons.buttons_left = 4;
                                tmr.stop();
                                tmr.time_elapsed = 0;
                            }
                        }
                    }
                    Button{
                        id : btn4
                        height: 100
                        width: 100
                        x:Math.random()*400
                        y:Math.random()*400
                        visible: false
                        onClicked: {
                            if(itmButtons.buttons_left > 1){
                                if(itmButtons.buttons_left == 4){
                                    lblTimeResult.text = "0.000s"
                                    tmr.start();
                                }
                                visible = false
                                btn1.visible = true
                                --itmButtons.buttons_left;
                            }
                            else{
                                btn1.visible = true;
                                visible = false
                                itmButtons.buttons_left = 4;
                                tmr.stop();
                                //tmr.time_elapsed = 0;
                            }
                        }
                    }
                }
                Label{
                    id : lblTimeResult
                    Layout.fillWidth: true
                    Layout.preferredHeight: 50
                    horizontalAlignment: Text.AlignHCenter
                    Layout.alignment: Qt.AlignHCenter + Qt.AlignVCenter
                    font.pixelSize: 50

                    text : "Result"
                    color: "green"
                }
                Timer{
                    id : tmr
                    interval: 100
                    running: false
                    repeat: true
                    property int time_elapsed : 0

                    onTriggered: {
                        time_elapsed += interval;
                        lblTimeResult.text = time_elapsed + "us";
                        if(time_elapsed < 1500)
                            lblTimeResult.color = "green";
                        else if(time_elapsed < 2000)
                            lblTimeResult.color = "yellow";
                        else
                            lblTimeResult.color = "red";
                    }
                }
            }
            footer: Button{
                text : "Next >"
                anchors.right: parent.right
                width: 200
                height: 100
                onClicked: {
                    swipeView.currentIndex = 8;
                    mainWindow.header.visible = false;
                }
            }
        }

        Page{// 3. главный экран с опциями
            ColumnLayout{
                anchors.fill: parent
                Button{
                    text: "Call"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Image{
                        height : 70
                        width : height
                        anchors.left: parent.left
                        anchors.margins: 5
                        anchors.verticalCenter: parent.verticalCenter
                        horizontalAlignment : Image.AlignHCenter
                        verticalAlignment : Image.AlignVCenter
                        source: "qrc:/img/call.png"//"qrc:/img/Group_81.png"//"qrc:/img/call.png"
                        fillMode : Image.PreserveAspectFit
                    }
                }
                Button{
                    text: "Message"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Image{
                        height : 70
                        width : height
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.margins: 5
                        horizontalAlignment : Image.AlignHCenter
                        verticalAlignment : Image.AlignVCenter
                        source: "qrc:/img/sms.png"//"qrc:/img/Group_80.png"//"qrc:/img/sms.png"
                        fillMode : Image.PreserveAspectFit
                    }
                }
                Button{
                    text: "Pharmacy"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Image{
                        height : 70
                        width : height
                        anchors.left: parent.left
                        anchors.margins: 5
                        anchors.verticalCenter: parent.verticalCenter
                        horizontalAlignment : Image.AlignHCenter
                        verticalAlignment : Image.AlignVCenter
                        source: "qrc:/img/bottle.png"//"qrc:/img/Group_2fgbfdtkb.png"//
                        fillMode : Image.PreserveAspectFit
                    }
                    onClicked: {
                        webView.visible = true;
                        swipeView.currentIndex = 7;
                        webView.update();
                    }
                }
                Button{
                    text: "Taxi"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Image{
                        height : 70
                        width : height
                        anchors.left: parent.left
                        anchors.margins: 5
                        anchors.verticalCenter: parent.verticalCenter
                        horizontalAlignment : Image.AlignHCenter
                        verticalAlignment : Image.AlignVCenter
                        source: "qrc:/img/taxi2.png"//"qrc:/img/Group_77.png"//"qrc:/img/taxi2.png"
                        fillMode : Image.PreserveAspectFit
                    }
                }
                RowLayout{
                    Button{
                        text: "Gesture\nunlock"
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        Image{
                            height : 70
                            width : height
                            anchors.left: parent.left
                            anchors.margins: 5
                            anchors.verticalCenter: parent.verticalCenter
                            horizontalAlignment : Image.AlignHCenter
                            verticalAlignment : Image.AlignVCenter
                            source: "qrc:/img/gestureunlock.png"
                            fillMode : Image.PreserveAspectFit
                        }
                        onClicked: {
                            swipeView.currentIndex = 4;

                        }
                    }
                    Button{
                        text: "Voice\nunlock"
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        Image{
                            height : 70
                            width : height
                            anchors.left: parent.left
                            anchors.margins: 5
                            anchors.verticalCenter: parent.verticalCenter
                            horizontalAlignment : Image.AlignHCenter
                            verticalAlignment : Image.AlignVCenter
                            source: "qrc:/img/voiceunlock.png"
                            fillMode : Image.PreserveAspectFit
                        }
                        onClicked: {
                            swipeView.currentIndex = 3;
                        }
                    }
                }
            }
        }

        Page{// 4. голосовая капча
            ColumnLayout{
                anchors.fill: parent
                Label{
                    text:"The quick brown fox jumps over the lazy dog"
                    wrapMode: Text.Wrap
                    font.pixelSize: 30
                    horizontalAlignment: Text.AlignHCenter
                    Layout.alignment: Qt.AlignHCenter + Qt.AlignVCenter
                    Layout.fillWidth: true
                }
                Button{
                    id : btnRec
                    //text:"Record"
                    Layout.preferredHeight: 100
                    Layout.preferredWidth: 100
                    Layout.alignment: Qt.AlignHCenter + Qt.AlignVCenter

                    onPressed: {
                        animation.start();
                        startRecord(true);
                    }

                    onReleased:{
                        animation.start();
                        startRecord(false);
                    }

                    background: Image{
                        id : btnRecImg
                        source: "qrc:/img/recording-button-png-8.png"
                        ScaleAnimator {
                            id:animation
                            target: btnRecImg // по отношению к какому элементу применяется анимация
                            easing.type: Easing.OutElastic // Easing.OutBounce // тип кривой анимации
                            from: 0.5; // стартовый масштаб 50%
                            to: 1; // финальный масштал 100%
                            duration: 500 // длительность
                            running: false // по умолчанию анимация остановлена
                        }
                    }


                }
            }
            footer: Button{
                text : "< Back"
                anchors.left: parent.left
                width: 150
                height: 80
                onClicked: {
                    swipeView.currentIndex = 2;
                }
            }
        }

        Page{// 5. проверка по точности жестов
                        ColumnLayout{
                            anchors.fill: parent
                            Label{
                                text: "Check your reaction to unlock"
                                Layout.fillWidth: true
                                Layout.preferredHeight: 100
                                wrapMode: Text.Wrap
                                font.pixelSize: 20
                            }
                            Item{
                                id : itmButtons2
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                                property int buttons_left: 4
                                Button{
                                    id : btn12
                                    height: 100
                                    width: 100
                                    visible: true
                                    x:Math.random()*400
                                    y:Math.random()*400

                                    onClicked: {
                                        if(itmButtons2.buttons_left > 1){
                                            if(itmButtons2.buttons_left == 4){
                                                lblTimeResult2.text = "0.000s"
                                                tmr12.start();
                                            }
                                            visible = false
                                            btn22.visible = true
                                            --itmButtons2.buttons_left;
                                        }
                                        else{
                                            btn12.visible = true;
                                            itmButtons2.buttons_left = 4;
                                            tmr12.stop();
                                            tmr12.time_elapsed = 0;
                                        }
                                    }
                                }
                                Button{
                                    id : btn22
                                    height: 100
                                    width: 100
                                    visible: false
                                    x:Math.random()*400
                                    y:Math.random()*400
                                    onClicked: {
                                        if(itmButtons2.buttons_left > 1){
                                            if(itmButtons2.buttons_left == 4){
                                                lblTimeResult2.text = "0.000s"
                                                tmr12.start();
                                            }
                                            visible = false
                                            btn32.visible = true
                                            --itmButtons2.buttons_left;
                                        }
                                        else{
                                            btn12.visible = true;
                                            itmButtons2.buttons_left = 4;
                                            tmr12.stop();
                                            tmr12.time_elapsed = 0;
                                        }
                                    }
                                }
                                Button{
                                    id : btn32
                                    height: 100
                                    width: 100
                                    x:Math.random()*400
                                    y:Math.random()*400
                                    visible: false
                                    onClicked: {
                                        if(itmButtons2.buttons_left > 1){
                                            if(itmButtons2.buttons_left == 4){
                                                lblTimeResult2.text = "0.000s"
                                                tmr12.start();
                                            }
                                            visible = false
                                            btn42.visible = true
                                            itmButtons2.buttons_left--;
                                        }
                                        else{
                                            btn12.visible = true;
                                            itmButtons2.buttons_left = 4;
                                            tmr12.stop();
                                            tmr12.time_elapsed = 0;
                                        }
                                    }
                                }
                                Button{
                                    id : btn42
                                    height: 100
                                    width: 100
                                    x:Math.random()*400
                                    y:Math.random()*400
                                    visible: false
                                    onClicked: {
                                        if(itmButtons2.buttons_left > 1){
                                            if(itmButtons2.buttons_left == 4){
                                                lblTimeResult2.text = "0.000s"
                                                tmr12.start();
                                            }
                                            visible = false
                                            btn12.visible = true
                                            --itmButtons2.buttons_left;

                                        }
                                        else{
                                            btn12.visible = true;
                                            visible = false
                                            itmButtons2.buttons_left = 4;
                                            tmr12.stop();
                                            if(tmr12.time_elapsed < tmr.time_elapsed)
                                            {
                                                swipeView.currentIndex = 8;
                                                mainWindow.header.visible = false;
                                            }
                                            tmr12.time_elapsed = 0;
                                        }
                                    }
                                }
                            }
                            Label{
                                id : lblTimeResult2
                                Layout.fillWidth: true
                                Layout.preferredHeight: 50
                                horizontalAlignment: Text.AlignHCenter
                                Layout.alignment: Qt.AlignHCenter + Qt.AlignVCenter
                                font.pixelSize: 50

                                text : "Result"
                                color: "green"
                            }
                            Timer{
                                id : tmr12
                                interval: 100
                                running: false
                                repeat: true
                                property int time_elapsed : 0

                                onTriggered: {
                                    time_elapsed += interval;
                                    lblTimeResult2.text = time_elapsed + "us";
                                    if(time_elapsed < 1500)
                                        lblTimeResult2.color = "green";
                                    else if(time_elapsed < 2000)
                                        lblTimeResult2.color = "yellow";
                                    else
                                        lblTimeResult2.color = "red";
                                }
                            }
                        }
                        footer: Button{
                            text : "Next >"
                            anchors.right: parent.right
                            width: 200
                            height: 100
                            onClicked: {
                                swipeView.currentIndex = 2;
                                mainWindow.header.visible = true;
                            }
                        }
        }

        Page{// 6. !падение и экстренный экран! акселерометр
            Rectangle{
                id : rct
                anchors.fill: parent
                color: "#00FF0000"
                ColumnLayout{
                    anchors.fill: parent

                    Label{
                        id : lblAlarm
                        text:"ALARM! APP DETECTED EXTREME FALL\nCALL FRIENDS IN\n"
                        wrapMode: Text.Wrap
                        font.pixelSize: 20
                        font.bold: true
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment : Text.AlignVCenter
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                    Timer{
                        id : tmrAlarm
                        property int countAlarm: 5000
                        interval: 500
                        repeat: true
                        running: false
                        triggeredOnStart: true
                        onTriggered: {
                            if(countAlarm <= 0){
                                tmrAlarm.stop();
                                if(Qt.platform.os === "android"){
                                    sendSMS("+79296090469",//"+79265830863"
                                            "Autogenerated by BoohApp: this user is drunk and needs assistance. https://her.is/2CMTjN0");
                                    makeCall("+79265830863");//makeCall("+79165882765");
                                    //sendSMS("+79165882765",
                                    //        "Autogenerated by BoohApp: this user is drunk and needs assistance");
                                }
                                lblAlarm.text = "CALLING EMEGRENCY CONTACTS!";
                                lblAlarm.color = "red"
                                countAlarm = 15000;
                                rct.color = "#00FF0000";
                            }else{
                                lblAlarm.text = "ALARM! APP DETECTED EXTREME FALL\nCALL FRIENDS IN\n" + countAlarm / 1000;
                                lblAlarm.color = "orange";
                                countAlarm -= 1000;
                            }
                            if(rct.color === "#00FF0000")
                                rct.color = "#FFFF0000";
                            else
                                rct.color = "#00FF0000";
                        }
                    }
                    Button{
                        text: "CANCEL\nAUTOCALL"
                        Layout.preferredHeight: Screen.pixelDensity * 20
                        Layout.alignment: Qt.AlignHCenter + Qt.AlignVCenter
                        onClicked: {
                            //if(tmrAlarm.running){
                                tmrAlarm.stop();
                                tmrAlarm.countAlarm = 15000;
                                lblAlarm.color = "green";
                                lblAlarm.text = "AUTO CALL WAS CANCELLED";
                                swipeView.currentIndex = 2;
                                rct.color = "#00FF0000"
                            /*}else{
                                tmrAlarm.start();
                                tmrAlarm.countAlarm = 15000;
                            }*/
                        }
                    }
                }

            }
        }

        Page{// 7-. акселерометр, график
            ColumnLayout{
                anchors.fill: parent
                /*ChartView {
                    id : chrt
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    antialiasing: true
                    ValueAxis {
                        id: axisX
                        min: 0
                        max: 30
                        tickCount: 5
                    }

                    ValueAxis {
                        id: axisY
                        min: 0
                        max: 50
                    }
                    LineSeries{
                        id: series1
                        axisX: axisX
                        axisY: axisY
                        //useOpenGL: true
                        XYPoint { x: 0; y: 0 }
                        XYPoint { x: 1; y: 10 }
                        XYPoint { x: 2; y: 20 }
                        XYPoint { x: 3; y: 30 }
                        XYPoint { x: 4; y: 10 }
                        XYPoint { x: 5; y: 20 }
                        XYPoint { x: 6; y: 30 }
                        XYPoint { x: 7; y: 10 }
                        XYPoint { x: 8; y: 20 }
                        XYPoint { x: 9; y: 30 }
                    }
                }

                Timer{
                    id : tmr3
                    running: true
                    repeat: true
                    interval: 10
                    property real tme: 0.0
                    onTriggered: {

                        for(var i = 0; i < 100; i++){
                            series1.replace(series1.at(i).x,
                                    series1.at(i).y,
                                    series1.at(i+1).x,
                                    series1.at(i+1).y);
                        }

                        series1.remove(100);
                        series1.append(100, time)
//                        replace(series1.at(99).x,
//                                series1.at(99).y,
//                                series1.at(i+1).x,
//                                series1.at(i+1).y)
                        chrt.update();
                    }
                }*/
                Label{
                    id : lblMaxAccel
                    text: "text"
                    verticalAlignment: Qt.AlignVCenter
                    horizontalAlignment: Qt.AlignHCenter
                    Layout.preferredHeight: 100
                    Layout.fillWidth: true
                    font.pixelSize: 30
                    font.bold: true
                }
                Label{
                    id : lblCurrentAccel
                    text: "text"
                    verticalAlignment: Qt.AlignVCenter
                    horizontalAlignment: Qt.AlignHCenter
                    Layout.preferredHeight: 100
                    Layout.fillWidth: true
                    font.pixelSize: 30
                    font.bold: true
                }
            }
        }

        Page{// 8. карта аптек
            //"https://wego.here.com/search/аптека?map=55.81623,37.58557,15,normal"

            /*WebView {
                id: webView
                anchors.fill: parent
                visible: false
                url: "https://wego.here.com/search/%25D0%25B0%25D0%25BF%25D1%2582%25D0%25B5%25D0%25BA%25D0%25B0?map=55.81746,37.58484,15,normal"
//                onLoadingChanged: {
//                    if (loadRequest.errorString)
//                        console.error(loadRequest.errorString);
//                }
            }*/
            footer: Button{
                text : "< Back"
                anchors.left: parent.left
                width: 150
                height: 80
                onClicked: {
                    swipeView.currentIndex = 2;
                    webView.visible = false;
                }
            }
        }

        Page{// 9
            Image{
                anchors.fill: parent
                source:"qrc:/img/desktop.jpg"
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    //TODO
                    popup.open();
                }
            }
            Popup {
                id: popup
                x: (parent.width - width)/2
                y: (parent.height - height)/2
                width: 200
                height: 300
                modal: true
                focus: true
                closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
                GridLayout{
                    anchors.fill: parent
                    Label{
                        text:"Activate safe mode?"
                        font.pixelSize: 20
                        wrapMode: Text.Wrap
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        Layout.column: 0
                        Layout.row: 0
                        Layout.columnSpan: 2
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                    Button{
                        text:"OK"
                        Layout.fillWidth: true
                        Layout.preferredHeight: 80
                        Layout.column: 0
                        Layout.row: 1
                        onClicked: {
                            popup.close();
                            swipeView.currentIndex = 2;
                            mainWindow.header.visible = true;
                        }
                    }
                    Button{
                        text:"Cancel"
                        Layout.fillWidth: true
                        Layout.preferredHeight: 80
                        Layout.column: 1
                        Layout.row: 1
                        onClicked: {
                            popup.close();
                        }
                    }
                }
            }
        }
    }

    /*footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Page 1")
        }
        TabButton {
            text: qsTr("Page 2")
        }
        TabButton {
            text: qsTr("Page 3")
        }
        TabButton {
            text: qsTr("Page 4")
        }
        TabButton {
            text: qsTr("Page 5")
        }
        TabButton {
            text: qsTr("Page 6")
        }
        TabButton {
            text: qsTr("Page 7")
        }
    }*/

    Accelerometer{
        id : accel
        active: (Qt.platform.os === "android")?true:false
        alwaysOn: (Qt.platform.os === "android")?true:false
        dataRate: 100
        property double max_accel: 0
        onReadingChanged: {
            var abs_accel = Math.sqrt(Math.pow(reading.x, 2)
                                      +Math.pow(reading.y, 2)
                                      +Math.pow(reading.z, 2));
            if(abs_accel > max_accel)
                max_accel = abs_accel;
            lblCurrentAccel.text = "Max accel = " + abs_accel;
            lblMaxAccel.text = "Max accel = " + max_accel;
            if(max_accel > 60){
                swipeView.currentIndex = 5;
                tmrAlarm.countAlarm = 15000;
                max_accel = 0;
                tmrAlarm.start();
            }

            /*series1.remove(0);
            series1.append(abs_accel);
            console.log(availableDataRates())*/

        }
    }
}
