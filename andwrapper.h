#ifndef ANDWRAPPER_H
#define ANDWRAPPER_H

#include <QObject>

class AndWrapper : public QObject
{
    Q_OBJECT
public:
    explicit AndWrapper(QObject *parent = nullptr);

signals:

public slots:
    void directCall(QString number);
    void sendSMS(QString number, QString message);
};

#endif // ANDWRAPPER_H
