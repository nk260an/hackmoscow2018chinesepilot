// https://stackoverflow.com/questions/29317048/how-to-make-a-call-with-qt-directly-from-the-application

#include "andwrapper.h"
#include <QDebug>
#ifdef Q_OS_ANDROID
    #include <QtAndroid>
    #include <QAndroidJniObject>
#endif
AndWrapper::AndWrapper(QObject *parent) : QObject(parent)
{

}

void AndWrapper::directCall(QString number)
{
#if defined(Q_OS_IOS)
    QDesktopServices::openUrl(QUrl(QString("tel://%1").arg(number)));
#elif defined(Q_OS_ANDROID)
    // get the Qt android activity
    QAndroidJniObject activity =
            QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative",
                                                      "activity",
                                                      "()Landroid/app/Activity;");
    //
    if (activity.isValid()){
    // real Java code to C++ code
    // Intent callIntent = new callIntent(Intent.ACTION_CALL);
    QAndroidJniObject callConstant =
            QAndroidJniObject::getStaticObjectField<jstring>("android/content/Intent",
                                                             "ACTION_CALL");
    QAndroidJniObject callIntent("android/content/Intent",
                                 "(Ljava/lang/String;)V",
                                 callConstant.object());
    // callIntent.setPackage("com.android.phone"); (<= 4.4w)  intent.setPackage("com.android.server.telecom");  (>= 5)
    QAndroidJniObject package;
    if(QtAndroid::androidSdkVersion() >= 21)
        package = QAndroidJniObject::fromString("com.android.server.telecom");
    else
        package = QAndroidJniObject::fromString("com.android.phone");
    callIntent.callObjectMethod("setPackage", "(Ljava/lang/String;)Landroid/content/Intent;", package.object<jstring>());
    // callIntent.setData(Uri.parse("tel:" + number));
    QAndroidJniObject jNumber = QAndroidJniObject::fromString(QString("tel:%1").arg(number));
    QAndroidJniObject uri =
            QAndroidJniObject::callStaticObjectMethod("android/net/Uri",
                                                      "parse",
                                                      "(Ljava/lang/String;)Landroid/net/Uri;",
                                                      jNumber.object());
    callIntent.callObjectMethod("setData",
                                "(Landroid/net/Uri;)Landroid/content/Intent;",
                                uri.object<jobject>());
    // callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    jint flag =
            QAndroidJniObject::getStaticField<jint>("android/content/Intent",
                                                    "FLAG_ACTIVITY_NEW_TASK");
    callIntent.callObjectMethod("setFlags", "(I)Landroid/content/Intent;", flag);
    //startActivity(callIntent);
    activity.callMethod<void>("startActivity",
                              "(Landroid/content/Intent;)V",
                              callIntent.object<jobject>());
}
    else
        qDebug() << "Something wrong with Qt activity...";
#else
    qDebug() << "Does nothing here...";
#endif
}

void AndWrapper::sendSMS(QString number, QString message)
{
    #ifdef Q_OS_ANDROID
    // get the Qt android activity
    QAndroidJniObject activity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative",
                                                                            "activity",
                                                                            "()Landroid/app/Activity;");
    if (activity.isValid()){

        //get the default SmsManager
        QAndroidJniObject mySmsManager = QAndroidJniObject::callStaticObjectMethod("android/telephony/SmsManager",
                                                                                   "getDefault",
                                                                                   "()Landroid/telephony/SmsManager;");
        if (!mySmsManager.isValid())
        {
            qDebug() << "Something wrong with SMS manager...";
        }
        else
        {
            // get phone number & text from UI and convert to Java String
            QAndroidJniObject myPhoneNumber = QAndroidJniObject::fromString(number);
            QAndroidJniObject myTextMessage = QAndroidJniObject::fromString(message);
            QAndroidJniObject scAddress = NULL;
            //QAndroidJniObject sentIntent = NULL;
            //QAndroidJniObject deliveryIntent = NULL;

            // call the java function:
            // public void SmsManager.sendTextMessage(String destinationAddress,
            //                                        String scAddress, String text,
            //                                        PendingIntent sentIntent, PendingIntent deliveryIntent)
            // see: [url]http://developer.android.com/reference/android/telephony/SmsManager.html[/url]

            mySmsManager.callMethod<void>("sendTextMessage",
                                          "(Ljava/lang/String;"
                                          "Ljava/lang/String;"
                                          "Ljava/lang/String;"
                                          "Landroid/app/PendingIntent;"
                                          "Landroid/app/PendingIntent;)V",
                                          myPhoneNumber.object<jstring>(),
                                          scAddress.object<jstring>(),
                                          myTextMessage.object<jstring>(), NULL, NULL );
        }

    }
    else
    {
        qDebug() << "Something wrong with Qt activity...";
    }
    #endif
}
