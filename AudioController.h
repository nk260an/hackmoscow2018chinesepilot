#ifndef AUDIOWORKER_H
#define AUDIOWORKER_H

#include <QAudioRecorder>
#include <QQuickItem>
#include <QMediaPlayer>

//Класс для работы с аудиозаметками, может создавать файлы аудио,
// начинать запись, ставить на паузу, проигрывать

class AudioController : public QQuickItem
{
    Q_PROPERTY(bool recordingStarted READ recordingStarted NOTIFY recordingStartedChanged)
    Q_OBJECT
public:
    AudioController();
    ~AudioController();
    bool recordingStarted() const { return m_recordingStarted;}

signals:
    void haveNewRecord(QString path, QString name);
    void recordingStartedChanged(bool recordStarted);

public slots:
    // включаем запись. Когда запись запускается, испускается сигнал haveNewRecord, который
    // обновляет список записей в графическом интерфейсе.
    void startRecording();
    // ставим запись на паузу(не работает на андроиде. Qt не поддерживает данный функционал для андроида)
    void pauseRecording();
    // останавливаем запись
    void stopRecording();

    // воспроизвести запись
    void playRecord(QString path);
    // воспроизвести текущую записть
    void playRecord();
    // остановить воспроизведение
    void stopRecord();
    // поставить воспроизведение на паузу
    void pauseRecord();
    // просканировать папку, в которой хранятся записи и сгенерировать из них список.
    void generateRecordsList();
    // удалить запись с диска
    void deleteRecord(QString path);
private:
    int recordCount;
    const QString recordsPath;
    bool m_recordingStarted;

    // сгенерировать новое название для файла
    QString generateRecordName();
    // сгенерировать путь до папки, в которой находятся все аудиозаписи
    QString generatePathToRecords();

    void setupRecorder();
    void setupPlayer();

    QAudioRecorder* audioRecorder;
    QMediaPlayer * player;

};

#endif // AUDIOWORKER_H
