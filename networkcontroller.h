#ifndef NETWORKCONTROLLER_H
#define NETWORKCONTROLLER_H

#include <QObject>
#include <QNetworkAccessManager>

class NetworkController : public QObject
{
    Q_OBJECT
public:
    explicit NetworkController(QObject *parent = nullptr);
    QNetworkAccessManager * na_manager;

signals:

public slots:
    void sendToServ(QByteArray);
};

#endif // NETWORKCONTROLLER_H
