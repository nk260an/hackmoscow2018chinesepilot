﻿#include "AudioController.h"
#include <QDebug>
#include <QDir>
#include <QStandardPaths>

/*!
    \class AudioController
    \brief Some class
    \since 5.9.1
    \ingroup tools
    \ingroup shared

    \reentrant

    \section1 Setting default text and capitalization

    To set the default text, auto-capitalized input and password mode - use the properties:

*/

AudioController::AudioController()
    :recordCount(0)
     ,recordsPath(generatePathToRecords())
     ,m_recordingStarted(false)
{
    setupRecorder();
    setupPlayer();
}

AudioController::~AudioController()
{
    if(audioRecorder->state() != QMediaRecorder::StoppedState) {
        audioRecorder->stop();
    }
    if(player->state() != QMediaPlayer::StoppedState) {
        audioRecorder->stop();
    }
    audioRecorder->deleteLater();
    player->deleteLater();
}

/*!
 * \brief Стартует запись аудиозаметки
 */
void AudioController::startRecording()
{
    QString name = generateRecordName();
    QString path = recordsPath + name;
    audioRecorder->setOutputLocation(QUrl::fromUserInput(path));
    emit haveNewRecord(path, name);
    audioRecorder->record();
    m_recordingStarted = true;
    emit recordingStartedChanged(m_recordingStarted);
}

/*!
 * \brief Ставит на паузу воспроизведение
 */
void AudioController::pauseRecording()
{
    audioRecorder->pause();

    m_recordingStarted = false;
    emit recordingStartedChanged(m_recordingStarted);
}

void AudioController::stopRecording()
{
    audioRecorder->stop();

    m_recordingStarted = false;
    emit recordingStartedChanged(m_recordingStarted);
}

void AudioController::playRecord(QString path)
{
    player->setMedia(QUrl::fromUserInput(path));
    player->play();
}

void AudioController::playRecord()
{
    player->play();
}

void AudioController::stopRecord()
{
    player->stop();
}

void AudioController::pauseRecord()
{
    player->pause();
}

void AudioController::generateRecordsList()
{
    QStringList NAME_FILTERS;
    NAME_FILTERS << "*.mp4*";
    QDir dir(recordsPath);
    QFileInfoList filesList = dir.entryInfoList(NAME_FILTERS, QDir::Files);
    for (int i = 0; i < filesList.count(); ++i) {
        QFileInfo file = filesList[i];
        recordCount++;
        emit haveNewRecord(file.absoluteFilePath(), file.fileName());
    }
}

void AudioController::deleteRecord(QString path)
{
    stopRecord();
    stopRecording();
    QDir().remove(path);
}

QString AudioController::generateRecordName()
{
    recordCount++;
    return QString().append("NewRecord_").append(QString::number(recordCount)).append(".mp4");
}

QString AudioController::generatePathToRecords()
{
    QString audiosFolder = QStandardPaths::writableLocation(QStandardPaths::MusicLocation) + "/Recorder/";
    QDir folder(audiosFolder);
    if(!folder.exists()){
        folder.mkpath(audiosFolder);
    }
    return audiosFolder;
}


void AudioController::setupRecorder()
{
    audioRecorder = new QAudioRecorder(this);
    QAudioEncoderSettings audioSettings;
    audioSettings.setCodec("audio/mp4");
    audioRecorder->setAudioInput(audioRecorder->defaultAudioInput());
    audioSettings.setQuality(QMultimedia::LowQuality);
    audioSettings.setBitRate(96000);
    audioRecorder->setEncodingSettings(audioSettings);


}

void AudioController::setupPlayer()
{
    player = new QMediaPlayer(this);
}
